import * as express from "express";
import * as https from "https";
import * as fs from "fs";
import * as bodyParser from "body-parser";
import * as log4js from "log4js";
import * as path from "path";

import routes from "./routes";

// init logger
const log = log4js.getLogger("main");
log.level = "debug";
log.trace("= TRACE IS ENABLED =");

log.info("Initializing application...");
const app = express();

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(routes);

// 404 Not found (bad URL)
app.use(function (req: express.Request, res: express.Response): any {
	return res.status(404).end("404 Not Found");
});

// 5xx Fatal error
app.use(function (err: any, req: express.Request, res: express.Response): any {
	if (err) {
		//TODO: send email, log err, etc...
		log.error(err);
	}
	return res.status(500).end("500 Internal Error");
});

// Make HTTP server instance
const server = https.createServer(
	{
		key: fs.readFileSync(path.join(__dirname, "../res/privkey.pem")),
		cert: fs.readFileSync(path.join(__dirname, "../res/fullchain.pem"))
	}, app);

// Register "listening" callback
server.on("listening", function (): any {
	const address = server.address();
	if (typeof address === "string") {
		log.info(`Server was started on ${address}`);
	} else {
		if (address) {
			log.info(address.family + " server was started on https://" + address.address + ":" + address.port);
		}
	}
});

const port: number = (process.env.PORT) ? parseInt(process.env.PORT) : 8089;
const listen: string = process.env.LISTEN || "localhost";

// Start listening
server.listen(port);

log.info("Starting HTTP Web Server...");
