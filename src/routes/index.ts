import * as express from "express";
import * as log4js from "log4js";
import * as path from "path";
import * as request from "request";
const jwtDecode = require("jwt-decode");


// Init logger
const log = log4js.getLogger("route");

const expressRouter: express.Router = express.Router();

expressRouter.get("/", (req, res, next) => {
	log.trace("Loading readme page...");
	res.status(200);
	return res.sendFile(path.join(__dirname, "index.html"));
});
expressRouter.get("/login", (req, res, next) => {
	log.trace("login...");
	const opts = helpers.conctructorOpts(req);

	helpers.getUrlForRedirectAuth(opts)
		.then((redirectUrl) => {
			return res.redirect(redirectUrl);
		})
		.catch((err) => {
			return res.status(500);
		});
});
expressRouter.get("/info", (req, res, next) => {
	log.trace("Info page");
	const jwtToken = helpers.parseJwt(req.query.id);
	res.status(200).send(jwtToken);
});


export namespace helpers {
	export function parseJwt(token: string) {
		return jwtDecode(token);
	}

	export function conctructorOpts(req: any) {
		// Check who entry provider or patient

		const role = "patient";

		const reqOpts = {
			url: `https://telehealth.apeasycloud.com/api/identity/${role}?redirectUrl=https://codeda.com:8089`,
			method: "GET",
			headers: { "Cache-Control": "no-cache" }
			// proxy: "http://localhost:3128"
		};
		return reqOpts;
	}

	export async function getUrlForRedirectAuth(opts: any) {
		return await promisifyRequest(opts);
	}

	export function promisifyRequest(opts: string): Promise<any> {
		return new Promise((resolve, reject) => {
			request(opts, function (err, response, body) {
				if (err) {
					return reject(err);
				} else {
					return resolve(body);
				}
			});
		});
	}
}

export default expressRouter;
